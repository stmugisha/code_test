from django.db import models
from django.contrib.auth.models import User

class StripeMember(models.Model):
    """
    The stripe customer model. It fetches customer subscription data
    from stripe and relates it back to the Django auth user
    """
    user = models.OneToOneField(to=User, on_delete=models.CASCADE)
    stripe_customer_id = models.CharField(max_length=128)
    stripe_subscription_id = models.CharField(max_length=128)

    def __str__(self):
        return self.user.username

