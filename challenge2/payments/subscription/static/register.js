//Makes fetch API calls to the stripe-conf/ endpoint

fetch('/stripe-conf/')
.then((result) => {return result.json(); })
.then((data) => {
    const stripe = Stripe(data.publicKey);
    
    //Add an event handler for when the subscribe button is clicked.
    let submitBtn = document.querySelector('#submitBtn'); //Get subscribe button by Id
    if(submitBtn !== null){
        submitBtn.addEventListener('click', () => {
            fetch('create-checkout-session/')
            .then((data) => {
                console.log(data);
                return stripe.redirectToCheckout({sessionId: data.sessionId})
            })
            .then((res) => {
                console.log(res);
            });
        });
    }
});
