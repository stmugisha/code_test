import stripe
from django.shortcuts import render
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings


@csrf_exempt
def stripe_conf(request):
    """stripe configuration endpoint."""
    if request.method == 'GET':
        stripe_config = {'publicKey': settings.STRIPE_PUBLISHABLE_KEY}
        return JsonResponse(stripe_config, safe=False)

@csrf_exempt
def gen_checkout_session(request):
    """Generates a new stripe Checkout session."""
    if request.method == 'GET':
        domain_url = 'https://localhost:8000/accounts/register/'
        stripe.api_key = settings.STRIPE_SECRET_KEY
        try:
            checkout_session = stripe.checkout.Session.create(
                client_reference_id = request.user.id if request.user.is_authenticated else None,
                success_url = domain_url + 'complete?session_id={CHECKOUT_SESSION_ID}',
                cancel_url = domain_url + 'closed/',
                payment_method_types = ['card'],
                mode = 'subscription',
                line_items = [
                    {
                        'price': settings.STRIPE_PRICE_ID,
                        'quantity': 1,
                    }
                ]
            )
            return JsonResponse({'session_id': checkout_session['id']})
        except Exception as e:
            return JsonResponse({'error': str(e)})
