# Test Challenge1 modules
import pytest
from compress import str_compress
from network import identify_router
    
@pytest.fixture
def c_data():
    """
    Defines and returns the data for the string compression
    and router identification tasks.
    """
    return[
        [
            "bbcceeee",
            "aaabbbcccaaa",
            "a"
        ],
        [
            "b2c2e4",
            "a6b3c3",
            "a"
        ],
        {
        1: [2],
        2: [3, 1],
        3: [5],
        5: [2]
        },
        {
        1: [3],
        3: [5],
        5: [6,2],
        6: [4],
        4: [5],
        2: [6]
        },
        {
        2: [4,5],
        4: [6],
        6: [2],
        5: [6]
        }
    ]

def test_str_compress(c_data):
    """
    Test that the str_compress method compresses a 
    string correctly.
    """
    assert str_compress(c_data[0][0])==c_data[1][0]
    assert str_compress(c_data[0][1])==c_data[1][1]
    assert str_compress(c_data[0][2])==c_data[1][2]

def test_router(c_data):
    """
    Test identify_router function
    """
    assert identify_router(c_data[2])=='2'
    assert identify_router(c_data[3])=='5'
    assert identify_router(c_data[4])=='2', '6'
