# String compression module

def str_compress(text: str) -> str:
    """
    Takes an input text string and returns unique letters 
    in the text along with their associated number of 
    occurances.
    """
    frequency_counter = {} #Initialize an empty dictionary to hold letter frequencies
    compressed_str = "" #Initialize an empty output string

    # lowercase the text to avoid duplicates due to case
    text = text.lower()
    
    for t in text: #loop through all letters in the string text
        #Exit the loop if an empty string is passed
        if len(text) < 1:
            break
        #If a single letter text is passed return the same letter and exit the loop
        elif len(text) == 1:
            return text
            break
        #Check if the a letter t (key) exists in the counter and increment its frequency (value) by 1
        if t in frequency_counter.keys():
            frequency_counter[t] += 1
        else:
            #Insert an entry of the letter into the counter if it doesn't exist yet.
            frequency_counter[t] = 1

    #Iterate through the frequency counter items and join the letters with nuber of times they occur
    #in the input text.
    for key, value in frequency_counter.items():
        if value > 1:
            compressed_str += "".join((str(key), str(value)))
        else:
            # Only add the letter the final output scleatring if the occurs once.
            compressed_str += str(key)
    return compressed_str

