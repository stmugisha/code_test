# Network Failure Problem

# Defining a network/mesh graph class to represent the mesh network stucture
class NGraph:
    def __init__(self, graph={}):
        """
        Initialize an empty dict for the graph object
        """
        self.graph = graph

    def vertices(self) -> list:
        """
        Returns a list of vertices/nodes in a graph
        """
        return list(self.graph.keys())

    def vertex_degree(self, vertex: str) -> int:
        """
        Calculates the degree of a vertex, which is the number of edges connecting it i.e.
        the number of adjacent vertices.
        """
        adjacent = self.graph[vertex]
        degree = len(adjacent) + adjacent.count(vertex)
        return degree


def identify_router(net: dict):
    """
    Returns the graph node(s) with the most number of
    connections in a graph network.
    """
    graph = NGraph(net) #Initialize a graph object
    num_connections = {}
    max_value = [] # Store nodes with the most/joint most connections

    #Get the number of connections per node and store in the num_collections dict
    for node in graph.vertices():
        num_connections[node] = graph.vertex_degree(node)

    #Iterate through the connections dict and get all nodes with the most connections
    for key, value in num_connections.items():
        if value == max(num_connections.values()):
            #Append the key to max_value list if its value is the maximum
            max_value.append(str(key))
    return ", ".join([i for i in max_value])

