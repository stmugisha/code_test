# Challenge1 #

### String Compression complexity:

Iterations (for loop) are performed n-times over an input string of length n and at each iteration, the frequency counter is updated. The frequency update operation happens in constant time (**O(1)**) with a worst case complexity of **O(n)** to search through a large number of dictionary keys and update their values. 
The second loop that iterates over all dictionary items also happens in **O(n)** time with n being the number of items.
Combining the three complexities above, we end up with a time complexity of **O(n) + O(1) + O(n) = O(2n+1) ~= O(n)**

### Router Identification:
Initializing the graph executes in constant time (O(1)) while the first loop that iterates over all vertices in the graph and calculates each of their vertex degree executes in a worst case **O(n)** time where n represents the number of vertices in the graph.
The second loop that finds key(s) with the maximum value also executes in a worst case **O(n)** time since it also iterates over all dictionary key-value pairs. n here would represent the number key-value pairs that exist in the graph/dictionary.

Combining all these executions yields a **O(1) + O(n) + O(n) = O(2n+1) ~= O(n)** time complexity
